class ChangeMonthColumnToInteger < ActiveRecord::Migration
  def change
    change_column :service_reports, :month, :integer
  end
end
