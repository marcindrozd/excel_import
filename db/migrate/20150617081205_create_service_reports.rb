class CreateServiceReports < ActiveRecord::Migration
  def change
    create_table :service_reports do |t|
      t.integer :report_id, :total_services
      t.string :month

      t.timestamps
    end
  end
end
