class Report < ActiveRecord::Base
  has_many :service_reports
  attr_accessor :file

  validates :title, presence: true
  validates :file, presence: true
end
