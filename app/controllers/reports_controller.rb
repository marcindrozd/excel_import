class ReportsController < ApplicationController
  def index
    @reports = Report.all
  end

  def new
    @report = Report.new
  end

  def create
    @report = Report.new(report_params)
    excel_file = params[:report][:file].tempfile if @report.valid?

    if @report.save
      ReportsImporter.handle_file(@report, excel_file)
      redirect_to @report, notice: "Report created successfully!"
    else
      render :new, alert: "Please check the errors below"
    end
  end

  def show
    @report = Report.find(params[:id])
    @service_reports = @report.service_reports
  end

  private

  def report_params
    params.require(:report).permit(:title, :file)
  end
end
