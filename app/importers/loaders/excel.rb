module Loaders::Excel
  def open_spreadsheet(file)
    case File.extname(file).downcase
      when '.xls' then Roo::Excel.new(file)
      when '.xlsx' then Roo::Excelx.new(file)
      else raise "Unknown file type: #{File.extname(file)}"
    end
  end

  def cell_string(col, row, sheet=@sheet)
    sheet.cell(col, row).to_s.strip
  end

  def cell_float(col, row, sheet=@sheet)
    sheet.cell(col, row).to_f
  end

  def cell_integer(col, row, sheet=@sheet)
    sheet.cell(col, row).to_i
  end

  def cell_date(col, row, format='%Y-%m-%d', sheet=@sheet)
    sheet.cell(col, row).strftime(format)
  end

  def cell_amount(col, row, decimal=true, sheet=@sheet)
    decimal ? sheet.cell(col, row).to_f : sheet.cell(col, row).gsub(/\D/, '').to_i
  end
end
