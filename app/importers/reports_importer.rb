class ReportsImporter
  extend Loaders::Excel

  SHEET = 'C01 Tiguan 2015'
  START_COLUMN = 'E'
  END_COLUMN = 'P'

  def self.handle_file(report_or_id, file)
    report = report_or_id.is_a?(Report) ? report_or_id : Report.find(params[:report_or_id])

    working_file = open_spreadsheet(file)
    working_sheet = working_file.sheet(SHEET)
    totals = []

    (START_COLUMN..END_COLUMN).each do |column|
      totals.push(working_sheet.column(column).count('x'))
    end

    update_report(report, totals)
  end

  def self.update_report(report, totals)
    service_reports_records = []
    totals.each_with_index do |total, idx|
      service_reports_records.push({month: idx + 1, total_services: total})
    end
    report.service_reports.create(service_reports_records)
  end
end
