Rails.application.routes.draw do
  root to: "reports#index"
  resources :reports, only: [:new, :create, :index, :show]
end
